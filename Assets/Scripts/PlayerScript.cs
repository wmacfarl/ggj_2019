﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{

    [SerializeField]
    private float speed;
    public int currentRoomID;
    public bool CanTeleport = true;
    public GameObject thigIAmCarrying = null;
    public Vector2 raycastOffset;
    public int leftRightFacing;
    public float raycastDistance = .7f;
    public float verticalDistanceBetweenRaycasts = .4f;
    public Vector2 carryPosition = new Vector2(1.05f, -.4f);
    public bool isHome;
    public ConversationManagerScript ConversationManager;
    public Canvas ConversationHUDPrefab;
    bool giveCoatDialogTriggered = false;
    bool canMove = true;

    bool coatDialogTriggered = false;
    bool lavalampDialogTriggered = false;
    bool couchDialogTriggered = false;
    bool friendMeetingDialogTriggered = false;
    bool fridgeDialogTriggered = false;

    string introDialogID = "intro";
    string fridgeDialogID = "fridge";
    string coatDialogID = "coat";
    string lavalampDialogID = "lavalamp";
    string couchDialogID = "couch";
    string giveCoatDialogID = "4";
    string friendMeetingDialogID = "2";

    public Sprite passing1;
    public Sprite passing2;
    public Sprite extreme1;
    public Sprite extreme2;
    public Sprite still;

    public Sprite passing1Holding;
    public Sprite passing2Holding;
    public Sprite extreme2Holding;
    public Sprite stillHolding;

    float lastAnimationTiming = 0;
    bool amWalking = false;

    float animationFrameLength = .2f;
    List<Sprite> holdingWalkCycle;
    List<Sprite> WalkCycle;
    int walkFrame = 0;

    // Use this for initialization
    void Start()
    {
        //    ConversationManager = Object.FindObjectOfType<ConversationManagerScript>();
        holdingWalkCycle = new List<Sprite>();
        WalkCycle = new List<Sprite>();
        holdingWalkCycle.Add(passing1Holding);
        holdingWalkCycle.Add(extreme1);
        holdingWalkCycle.Add(passing2Holding);
        holdingWalkCycle.Add(extreme2Holding);

        WalkCycle.Add(passing1);
        WalkCycle.Add(extreme1);
        WalkCycle.Add(passing2);
        WalkCycle.Add(extreme2);
        lastAnimationTiming = Time.time;
    }


    void AnimateSprites()
    {
        SpriteRenderer mySprite = GetComponent<SpriteRenderer>();
        if (amWalking)
        {
            if (Time.time-lastAnimationTiming > animationFrameLength)
            {
                walkFrame++;
                if (walkFrame >= WalkCycle.Count)
                {
                    walkFrame = 0;
                }
                lastAnimationTiming = Time.time;
            }
            List<Sprite> currentWalkCycle;
            if (thigIAmCarrying == null)
            {
                currentWalkCycle = WalkCycle;
            }
            else
            {
                currentWalkCycle = holdingWalkCycle;
            }
            mySprite.sprite = currentWalkCycle[walkFrame];
        }
        else
        {
            walkFrame = 0;
            if (thigIAmCarrying != null)
            {
                mySprite.sprite = stillHolding;
            }
            else
            {
                mySprite.sprite = still;
            }
        }
    }

    private void Update()
    {
        AnimateSprites();
    }
    // Update is called once per frame
    void FixedUpdate()
    {    if (ConversationManager != null)
        {
            canMove = !ConversationManager.isDialogActive();
        }
        float horizontal = 0;
        float vertical = 0;
        if (canMove)
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                vertical = 1;
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                leftRightFacing = 1;
                horizontal = 1;
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                vertical = -1;
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                leftRightFacing = -1;
                horizontal = -1;
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                pickUpOrPutDown();
            }
        }
        GetComponent<Rigidbody2D>().velocity = new Vector2(horizontal * speed, vertical * speed);
        if (horizontal != 0 || vertical != 0)
        {
            amWalking = true;
        }
        else
        {
            amWalking = false;
        }


        GetComponent<SpriteRenderer>().flipX = (leftRightFacing == -1);
        if (thigIAmCarrying != null)
        {
            thigIAmCarrying.transform.localPosition = new Vector2(leftRightFacing * carryPosition.x, carryPosition.y);
        }
    }

    public void pickUpOrPutDown()
    {
        if (thigIAmCarrying == null)
        {
            tryToPickUp();
        }
        else
        {
            tryToPutDown();
        }
    }

    public void tryToPutDown()
    {
        thigIAmCarrying.transform.parent = null;
        thigIAmCarrying = null;
    }
    public void tryToPickUp()
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll((Vector2)transform.position + new Vector2(raycastOffset.x * leftRightFacing, raycastOffset.y), new Vector2(leftRightFacing, 0), raycastDistance);
        RaycastHit2D[] hits2 = Physics2D.RaycastAll((Vector2)transform.position + new Vector2(raycastOffset.x * leftRightFacing, raycastOffset.y + verticalDistanceBetweenRaycasts), new Vector2(leftRightFacing, 0), raycastDistance);
        RaycastHit2D[] hits3 = Physics2D.RaycastAll((Vector2)transform.position + new Vector2(raycastOffset.x * leftRightFacing, raycastOffset.y - verticalDistanceBetweenRaycasts), new Vector2(leftRightFacing, 0), raycastDistance);

        Debug.DrawRay(transform.position + new Vector3(raycastOffset.x * leftRightFacing, raycastOffset.y), new Vector2(leftRightFacing, 0) * raycastDistance, Color.green);
        Debug.DrawRay(transform.position + new Vector3(raycastOffset.x * leftRightFacing, raycastOffset.y + verticalDistanceBetweenRaycasts), new Vector2(leftRightFacing, 0) * raycastDistance, Color.green);
        Debug.DrawRay(transform.position + new Vector3(raycastOffset.x * leftRightFacing, raycastOffset.y - verticalDistanceBetweenRaycasts), new Vector2(leftRightFacing, 0) * raycastDistance, Color.green);
        List<RaycastHit2D> allHits = new List<RaycastHit2D>();
        allHits.AddRange(hits);
        allHits.AddRange(hits3);
        allHits.AddRange(hits2);

        if (allHits.Count > 0)
        {
            foreach (RaycastHit2D hit in allHits)
            {
                GameObject hitObject = hit.collider.gameObject;
                if (hitObject.GetComponent<CarryableScript>() != null)
                {

                    hitObject.transform.parent = transform;
                    hitObject.transform.localPosition = new Vector3(.5f * leftRightFacing, -.25f, 0);
                    thigIAmCarrying = hitObject;
                    break;
                }
            }
        }
    }




    public IEnumerator ActivateTeleport()
    {
        CanTeleport = false;
        yield return new WaitForSeconds(2);
        CanTeleport = true;
    }

    public void SwitchRooms(int newRoomID)
    {
        int previousRoomID = currentRoomID;
        currentRoomID = newRoomID;
        RoomScript[] allRoomScripts = Object.FindObjectsOfType<RoomScript>();
        foreach (RoomScript room in allRoomScripts)
        {
            if (room.RoomID != currentRoomID)
            {
                room.TurnOffSpriteMasks();
            }
        }
        if (newRoomID == 0 && previousRoomID != 0)
        {
            Object.FindObjectOfType<AudioManagerScript>().setHomeStatus(true);
        }else if (newRoomID != 0 && previousRoomID == 0)
        {
            Object.FindObjectOfType<AudioManagerScript>().setHomeStatus(false);

        }

    }

    public void StartGame()
    {
        ConversationManager = Object.FindObjectOfType<ConversationManagerScript>();
        StartCoroutine(TutorialDialog());
    }



    IEnumerator TutorialDialog()
    {
        yield return new WaitForSeconds(3);
        print(Time.time);
        ConversationManager.startDialog("intro");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<LavaLampScript>() != null)
        {
            if (!lavalampDialogTriggered)
            {
                ConversationManager.startDialog(lavalampDialogID);
                lavalampDialogTriggered = true;
            }
        }else if (collision.gameObject.GetComponent<CoatScript>() != null)
        {
            if (!coatDialogTriggered)
            {
                ConversationManager.startDialog(coatDialogID);
                coatDialogTriggered = true;
            }

        }
        else if (collision.gameObject.GetComponent<CouchScript>() != null)
        {
            if (!couchDialogTriggered)
            {
                ConversationManager.startDialog(couchDialogID);
                couchDialogTriggered = true;
            }

        }
        else if (collision.gameObject.GetComponent<MiniFridgeScript>() != null)
        {
            if (!fridgeDialogTriggered)
            {
                ConversationManager.startDialog(fridgeDialogID);
                fridgeDialogTriggered = true;
            }

        }
        else if (collision.gameObject.GetComponent<FriendScript>() != null)
        {
            if (!friendMeetingDialogTriggered)
            {
                ConversationManager.startDialog(friendMeetingDialogID);
                friendMeetingDialogTriggered = true;
            }
            else
            {
                if (thigIAmCarrying.GetComponent<CoatScript>() != null  && !giveCoatDialogTriggered)
                {
                    ConversationManager.startDialog(giveCoatDialogID);
                    giveCoatDialogTriggered = true;
                }
            }


        }
    }


}