﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;

public class DeckGeneratorScript : MonoBehaviour
{

    public bool shipIsGood = true;

    public GameObject shipGridPrefab;
    public GameObject CouchPrefab;
    public GameObject FriendPrefab;
    public GameObject FridgePrefab;
    public GameObject LavalampPrefab;
    public GameObject CoatPrefab;

    [SerializeField]
    private Tile roomFloorTile;

    [SerializeField]
    private Tile hallwayFloorTile;

    [SerializeField]
    private Tile horizontalDoorTile;

    [SerializeField]
    private Tile verticalDoorTile;

    [SerializeField]
    private Tile wallTile;

    [SerializeField]
    private Tilemap tileMap;

    [SerializeField]
    private GameObject player;

    [SerializeField]
    private int numberOfRooms = 10;

    [SerializeField]
    private int numberOfDecks = 4;

    [SerializeField]
    private int deckLength = 200;

    [SerializeField]
    private int deckHeight = 50;

    [SerializeField]
    private int maxRoomLength = 30;

    [SerializeField]
    private int maxRoomHeight = 10;

    [SerializeField]
    private int minRoomLength = 8;

    [SerializeField]
    private int minRoomHeight = 4;

    [SerializeField]
    private int hallwayLengthMin = 8;

    [SerializeField]
    private int hallwayLengthMax = 40;

    [SerializeField]
    private int hallwayWidthMin = 3;

    [SerializeField]
    private int hallwayWidthMax = 8;

    [SerializeField]
    private GameObject HorizontalDoorPrefab;

    [SerializeField]
    private GameObject VerticalDoorPrefab;

    [SerializeField]
    private GameObject RoomPrefab;

    [SerializeField]
    private GameObject PlayerPrefab;

    [SerializeField]
    private GameObject TeleporterPrefab;

    [SerializeField]
    private GameObject CratePrefab;

    [SerializeField]
    private GameObject BarrelPrefab;

    [SerializeField]
    Tile EastWallTile;

    [SerializeField]
    Tile WestWallTile;

    [SerializeField]
    Tile NorthWallInnerTile;

    [SerializeField]
    Tile NorthWallOuterTile;

    [SerializeField]
    Tile SouthWallTile;

    [SerializeField]
    Tile InteriorWallTile;

    [SerializeField]
    Tile NorthEastWallTile;

    [SerializeField]
    Tile SouthEastWallTile;

    [SerializeField]
    Tile NorthWestWallTile;

    [SerializeField]
    Tile SouthWestWallTile;

    public List<GameObject> RoomGameObjects;
    public List<GameObject> HallwayGameObjects;
    
    public List<Deck> Decks;

    [SerializeField]
    private GameObject DeckPrefab;

    public GameObject ship;

    [SerializeField]
    GameObject ShipPrefab;

    [SerializeField]
    GameObject HallwayPrefab;

    public int deckNumber = 0;

    const int NORTH = 0;
    const int SOUTH = 1;
    const int EAST = 2;
    const int WEST = 3;

    public int nextRoomID = 0;

    public List<GameObject> allDoors;

    public class Deck
    {
        public int deckNumber;
        public List<Room> rooms;
        public GameObject DeckGameObject;

        public Deck(int deckNumber)
        {
            this.rooms = new List<Room>();
            this.deckNumber = deckNumber;
        }
    }

    private void Start()
    {
        GenerateShip();
        if (!shipIsGood)
        {
            return;
        }
        GameObject.Find("Player").GetComponent<PlayerScript>().StartGame();

    }



    void GenerateHallwayGameObjects()
    {
        foreach (GameObject door in allDoors)
        {
            DoorScript doorScript = door.GetComponent<DoorScript>();
            if (doorScript.matchingDoor == null)
            {
                doorScript.matchingDoor = FindMatchingDoor(door);
                if (doorScript.matchingDoor == null)
                {
                    doorScript.matchingDoor = door;
                }
                DoorScript otherDoorScript = doorScript.matchingDoor.GetComponent<DoorScript>();
                otherDoorScript.matchingDoor = doorScript.gameObject;
                GameObject newHallway = GameObject.Instantiate(HallwayPrefab);
                otherDoorScript.attachedHallway = newHallway;
                doorScript.attachedHallway = newHallway;
                newHallway.GetComponent<HallwayScript>().door1 = door;
                newHallway.GetComponent<HallwayScript>().door2 = doorScript.matchingDoor;
                Vector2 pos = (door.transform.position + doorScript.matchingDoor.transform.position) / 2.0f;
                pos.x += 1;
                pos.y -= 1;
                newHallway.transform.position = pos;
                BoxCollider2D hallCollider = newHallway.GetComponent<BoxCollider2D>();
                hallCollider.size = new Vector2(Mathf.Abs(door.transform.position.x - doorScript.matchingDoor.transform.position.x),
                    Mathf.Abs(door.transform.position.y - doorScript.matchingDoor.transform.position.y));
                if (doorScript.DoorType == doorScript.VERTICAL)
                {
                    newHallway.transform.Translate(new Vector2(.5f, .5f));
                    hallCollider.size = new Vector2(3, hallCollider.size.y+1);
                }
                else
                {
                    newHallway.transform.Translate(new Vector2(-.5f, -.5f));
                    hallCollider.size = new Vector2(hallCollider.size.x+1, 3);
                }
                if (door != null && newHallway != null && door.transform.parent != null)
                {
                    newHallway.transform.parent = door.transform.parent.parent;
                }
                else
                {
                    shipIsGood = false;
                    return;
                }
            }
            
        }
    }

    public GameObject FindMatchingDoor(GameObject door)
    {
        GameObject matchingDoor = null;
        DoorScript doorScript = door.GetComponent<DoorScript>();
        if (doorScript.DoorType == doorScript.VERTICAL) {
            List<GameObject> doorsOnSameColumn = new List<GameObject>();
            for (int i = 0; i < allDoors.Count; i++)
            {
                GameObject otherDoor = allDoors[i];
                if (door != otherDoor)
                {
                    if (door.transform.position.x == otherDoor.transform.position.x)
                    {
                        if (doorScript.DoorSide == -1 && door.transform.position.y < otherDoor.transform.position.y)
                        {
                            doorsOnSameColumn.Add(otherDoor);
                        }
                        else if (doorScript.DoorSide == 1 && door.transform.position.y > otherDoor.transform.position.y) {
                            doorsOnSameColumn.Add(otherDoor);
                        }
                    }
                }
            }
            if (doorsOnSameColumn.Count > 0)
            {
                GameObject closetDoorOnSameColumn = doorsOnSameColumn[0];
                float distance = Mathf.Abs(door.transform.position.y - closetDoorOnSameColumn.transform.position.y);
                foreach (GameObject doorToCheck in doorsOnSameColumn)
                {
                    float checkDistance = Mathf.Abs(door.transform.position.y - doorToCheck.transform.position.y);
                    if (checkDistance < distance)
                    {
                        distance = checkDistance;
                        closetDoorOnSameColumn = doorToCheck;
                    }
                }

                return closetDoorOnSameColumn;
            }
            else
            {
                Debug.Log("FOUND NO OTHER DOOR");
                shipIsGood = false;
                return null;
            }
        }
        else
        {
            List<GameObject> doorsOnSameRow = new List<GameObject>();
            for (int i = 0; i < allDoors.Count; i++)
            {
                GameObject otherDoor = allDoors[i];
                if (door != otherDoor)
                {
                    if (door.transform.position.y == otherDoor.transform.position.y)
                    {
                        if (doorScript.DoorSide == -1 && door.transform.position.x < otherDoor.transform.position.x)
                        {
                            doorsOnSameRow.Add(otherDoor);
                        }
                        else if (doorScript.DoorSide == 1 && door.transform.position.x > otherDoor.transform.position.x)
                        {
                            doorsOnSameRow.Add(otherDoor);
                        }
                    }
                }
            }
            if (doorsOnSameRow.Count > 0)
            {
                GameObject closetDoorOnSameRow = doorsOnSameRow[0];
                float distance = Mathf.Abs(door.transform.position.x - closetDoorOnSameRow.transform.position.x);
                foreach (GameObject doorToCheck in doorsOnSameRow)
                {
                    float checkDistance = Mathf.Abs(door.transform.position.x - doorToCheck.transform.position.x);
                    if (checkDistance < distance)
                    {
                        distance = checkDistance;
                        closetDoorOnSameRow = doorToCheck;
                    }
                }
                return closetDoorOnSameRow;
            }
            else
            {
                Debug.Log("FOUND NO OTHER DOOR");
                shipIsGood = false;
                return null;
            }
        }
    }



    public void GenerateTeleporters()
    {
        for (int i = 0; i < Decks.Count; i++)
        {
            Deck deck = Decks[i];
            if (i + 1 < Decks.Count)
            {
                Deck nextDeck = Decks[i + 1];
                RoomScript[] roomsOnDeck = deck.DeckGameObject.GetComponentsInChildren<RoomScript>();
                RoomScript[] roomsOnNextDeck = nextDeck.DeckGameObject.GetComponentsInChildren<RoomScript>();

                GameObject teleporter1 = GameObject.Instantiate(TeleporterPrefab);
                GameObject room1 = roomsOnDeck[Random.Range(0, roomsOnDeck.Length)].gameObject;
                teleporter1.transform.position = room1.transform.position;
                teleporter1.transform.parent = room1.transform;
                room1.GetComponent<RoomScript>().objectsInRoom.Add(teleporter1);

                GameObject teleporter2 = GameObject.Instantiate(TeleporterPrefab);
                GameObject room2 = roomsOnNextDeck[Random.Range(0, roomsOnNextDeck.Length)].gameObject;
                teleporter2.transform.position = room2.transform.position;
                teleporter2.transform.parent = room2.transform;
                room2.GetComponent<RoomScript>().objectsInRoom.Add(teleporter2);

                teleporter1.GetComponent<TeleporterScript>().otherSide = teleporter2;
                teleporter2.GetComponent<TeleporterScript>().otherSide = teleporter1;
            }
        }
    }

    public void CombineRoomGameObjects()
    {
        int iterations = 0;
        bool stillCombining = true;
        while (stillCombining && iterations < 10000) 
        {
            stillCombining = false;
            iterations++;
            for (int i = 0; i < RoomGameObjects.Count; i++)
            {
                for (int j = i + 1; j < RoomGameObjects.Count; j++)
                {
                    
                    BoxCollider2D collider1 = RoomGameObjects[i].GetComponent<BoxCollider2D>();
                    BoxCollider2D collider2 = RoomGameObjects[j].GetComponent<BoxCollider2D>();
                    RoomScript roomScript1 = RoomGameObjects[i].GetComponent<RoomScript>();
                    RoomScript roomScript2 = RoomGameObjects[j].GetComponent<RoomScript>();

                    if (collider1.bounds.Intersects(collider2.bounds) && roomScript1.RoomID != roomScript2.RoomID)
                    {
                       if (roomScript1.RoomID < roomScript2.RoomID)
                       {
                           roomScript2.RoomID = roomScript1.RoomID;
                            roomScript2.transform.parent = roomScript1.transform;
                       }
                       else
                       {
                          roomScript1.RoomID = roomScript2.RoomID;
                            roomScript1.transform.parent = roomScript2.transform;
                        }
                        stillCombining = true;
                    }
                }
            }
        }
    }

    GameObject CreateHallwayGameObject(GameObject door1, GameObject door2)
    {
        return null;
    }


    public class Room
    {
        public Vector2Int position;
        public Vector2Int dimensions;
        public bool[] hasHallway;
        public Deck deck;

        public Room(Vector2Int position, Vector2Int dimensions, Deck deck)
        {
            this.deck = deck;
            if (this.dimensions.x < 0)
            {
                Debug.Log("negative x dimension");
            }
            if (this.dimensions.y < 0)
            {
                Debug.Log("negative x dimension");
            }
            this.position = position;
            this.dimensions = dimensions;

            hasHallway = new bool[4];
            for (int i = 0; i < 4; i++)
            {
                hasHallway[i] = false;
            }
        }


        public int GetTopY()
        {
            return position.y + dimensions.y;
        }

        public int GetBottomY()
        {
            return position.y;
        }

        public int GetLeftX()
        {
            return position.x;
        }

        public int GetRightX()
        {
            return position.x + dimensions.x;
        }

        public Vector2Int[] getCorners()
        {
            Vector2Int[] corners = new Vector2Int[4];
            corners[0] = new Vector2Int(GetLeftX(), GetTopY());
            corners[1] = new Vector2Int(GetRightX(), GetTopY());
            corners[2] = new Vector2Int(GetRightX(), GetBottomY());
            corners[3] = new Vector2Int(GetRightX(), GetBottomY());
            return corners;
        }


    }

    class Hallway
    {
        Vector2Int position;
        public List<Room> connectedRooms;
        public int direction;
        public int length;
        public Hallway(Room startingRoom, int direction, int length, Vector2Int position)
        {
            this.position = position;
            connectedRooms = new List<Room>();
            connectedRooms.Add(startingRoom);
            this.direction = direction;
            this.length = length;
       }

        public int getEndX()
        {
            if (direction == EAST) {
                return position.x + length;
            }
            else if (direction == WEST)
            {
                return position.x - length;
            }
            else
            {
                return position.x;
            }
        }

        public int getEndY()
        {
            if (direction == NORTH)
            {
                return position.y + length;
            }
            else if (direction == SOUTH)
            {
                return position.y - length;
            }
            else
            {
                return position.y;
            }

        }
    }

    public bool doRoomsIntersect(Room room1, Room room2)
    {
        foreach (Vector2Int room1Corner in room1.getCorners()){
            if (room1Corner.x <= room2.GetRightX() && room1Corner.x >= room2.GetLeftX() && room1Corner.y <= room2.GetTopY() 
                && room1Corner.y >= room2.GetBottomY())
            {
                return true;
            }
        }

        foreach (Vector2Int room2Corner in room2.getCorners()){
            if (room2Corner.x <= room1.GetRightX() && room2Corner.x >= room1.GetLeftX() && room2Corner.y <= room1.GetTopY() 
                && room2Corner.y >= room1.GetBottomY())
            {
                return true;
            }
        }

        return false;
    }

    private void FillWalls()
    {
        BoundsInt bounds = tileMap.cellBounds;

        //First fill base walls.
        for (int xMap = bounds.xMin - 1; xMap <= bounds.xMax + 1; xMap++)
        {
            for (int yMap = bounds.yMin - 1; yMap <= bounds.yMax + 1; yMap++)
            {
                Vector3Int pos = new Vector3Int(xMap, yMap, 0);
                Vector3Int posBelow = new Vector3Int(xMap, yMap - 1, 0);
                Vector3Int posAbove = new Vector3Int(xMap, yMap + 1, 0);
                Vector3Int posLeft = new Vector3Int(xMap - 1, yMap, 0);
                Vector3Int posRight = new Vector3Int(xMap + 1, yMap, 0);
                TileBase tile = tileMap.GetTile(pos);
                TileBase tileBelow = tileMap.GetTile(posBelow);
                TileBase tileAbove = tileMap.GetTile(posAbove);
                TileBase tileLeft = tileMap.GetTile(posLeft);
                TileBase tileRight = tileMap.GetTile(posRight);

                if (tile == null)
                {
                    if (tileAbove == null && (tileBelow == hallwayFloorTile || tileBelow == roomFloorTile))
                    {
                        tileMap.SetTile(pos, NorthWallInnerTile);
                        tileMap.SetTile(posAbove, NorthWallOuterTile);
                    }
                    else if ((tileBelow == null || tileBelow == NorthWallOuterTile) && (tileAbove == hallwayFloorTile || tileAbove == roomFloorTile))
                    {
                        tileMap.SetTile(pos, SouthWallTile);
                    }
                    else if (tileLeft == null && (tileRight == hallwayFloorTile || tileRight == roomFloorTile))
                    {
                        tileMap.SetTile(pos, WestWallTile);
                    }
                    else if (tileRight == null && (tileLeft == hallwayFloorTile || tileLeft == roomFloorTile))
                    {
                        tileMap.SetTile(pos, EastWallTile);
                    }

                }
            }
        }
        for (int xMap = bounds.xMin - 1; xMap <= bounds.xMax + 1; xMap++)
        {
            for (int yMap = bounds.yMin - 1; yMap <= bounds.yMax + 1; yMap++)
            {
                Vector3Int pos = new Vector3Int(xMap, yMap, 0);
                Vector3Int posBelow = new Vector3Int(xMap, yMap - 1, 0);
                Vector3Int posAbove = new Vector3Int(xMap, yMap + 1, 0);
                Vector3Int posLeft = new Vector3Int(xMap - 1, yMap, 0);
                Vector3Int posRight = new Vector3Int(xMap + 1, yMap, 0);
                TileBase tile = tileMap.GetTile(pos);
                TileBase tileBelow = tileMap.GetTile(posBelow);
                TileBase tileAbove = tileMap.GetTile(posAbove);
                TileBase tileLeft = tileMap.GetTile(posLeft);
                TileBase tileRight = tileMap.GetTile(posRight);

                if (tile == null)
                {
                    if (tileBelow == WestWallTile)
                    {
                        tileMap.SetTile(pos, WestWallTile);
                 //       tileMap.SetTile(posAbove, NorthWestWallTile);
                    }
                    if (tileBelow == EastWallTile)
                    {
                        tileMap.SetTile(posAbove, NorthEastWallTile);
                   //     tileMap.SetTile(pos, EastWallTile);
                    }
                    if (tileAbove == WestWallTile)
                    {
                        tileMap.SetTile(pos, SouthWestWallTile);
                    }
                    if (tileAbove == EastWallTile)
                    {
                        tileMap.SetTile(pos, SouthEastWallTile);
                    }
                    if (tileLeft == NorthWallInnerTile)
                    {
                        tileMap.SetTile(pos, EastWallTile);
                    }
                    if (tileLeft == NorthWallOuterTile)
                    {
                        tileMap.SetTile(pos, NorthEastWallTile);
                    }
                    if (tileRight == NorthWallInnerTile)
                    {
                        tileMap.SetTile(pos, WestWallTile);
                    }
                    if (tileRight == NorthWallOuterTile)
                    {
                        tileMap.SetTile(pos, NorthWestWallTile);
                    }
                }
            }
        }
        for (int xMap = bounds.xMin - 1; xMap <= bounds.xMax + 1; xMap++)
        {
            for (int yMap = bounds.yMin - 1; yMap <= bounds.yMax + 1; yMap++)
            {
                Vector3Int pos = new Vector3Int(xMap, yMap, 0);
                Vector3Int posBelow = new Vector3Int(xMap, yMap - 1, 0);
                Vector3Int posAbove = new Vector3Int(xMap, yMap + 1, 0);
                Vector3Int posLeft = new Vector3Int(xMap - 1, yMap, 0);
                Vector3Int posRight = new Vector3Int(xMap + 1, yMap, 0);
                TileBase tile = tileMap.GetTile(pos);
                TileBase tileBelow = tileMap.GetTile(posBelow);
                TileBase tileAbove = tileMap.GetTile(posAbove);
                TileBase tileLeft = tileMap.GetTile(posLeft);
                TileBase tileRight = tileMap.GetTile(posRight);

                if (tile == null)
                {
                    if (isWallTile(tileBelow) || isWallTile(tileAbove) || isWallTile(tileLeft) || isWallTile(tileRight))
                    {
                        tileMap.SetTile(pos, InteriorWallTile);
                    }
                }
            }
        }
    }

    bool isWallTile(TileBase tile)
    {
        return (tile == NorthEastWallTile || tile == NorthWestWallTile || tile == NorthWallInnerTile || tile == EastWallTile || tile == WestWallTile || tile == wallTile || tile == NorthWallOuterTile);
    }

    private void GenerateDoors()
    {
        allDoors = new List<GameObject>();
        BoundsInt bounds = tileMap.cellBounds;
        for (int xMap = bounds.xMin - 10; xMap <= bounds.xMax + 10; xMap++)
        {
            for (int yMap = bounds.yMin - 10; yMap <= bounds.yMax + 10; yMap++)
            {
                Vector3Int pos = new Vector3Int(xMap, yMap, 0);
                Vector3Int posBelow = new Vector3Int(xMap, yMap - 1, 0);
                Vector3Int posAbove = new Vector3Int(xMap, yMap + 1, 0);
                Vector3Int posLeft = new Vector3Int(xMap-1, yMap, 0);
                Vector3Int posRight = new Vector3Int(xMap+1, yMap, 0);
                TileBase tile = tileMap.GetTile(pos);
                TileBase tileBelow = tileMap.GetTile(posBelow);
                TileBase tileAbove = tileMap.GetTile(posAbove);
                TileBase tileLeft = tileMap.GetTile(posLeft);
                TileBase tileRight = tileMap.GetTile(posRight);
                if (tile == hallwayFloorTile)
                {
                    if ((tileAbove == roomFloorTile || tileBelow == roomFloorTile) && isWallTile(tileLeft) && tileRight == hallwayFloorTile && isWallTile(tileBelow) == false && isWallTile(tileAbove) == false)
                    {
                   //     tileMap.SetTile(pos, verticalDoorTile);
                       GameObject newDoor =  GameObject.Instantiate(VerticalDoorPrefab);
                        allDoors.Add(newDoor);
                        newDoor.transform.position = tileMap.CellToWorld(posAbove);
                        DoorScript newDoorScript = newDoor.GetComponent<DoorScript>();
                        newDoorScript.DoorType = newDoorScript.VERTICAL;
                        if (tileMap.GetTile(posBelow) == roomFloorTile)
                        {
                            RaycastHit2D[] hitsBelowDoor = Physics2D.RaycastAll(newDoor.transform.position, Vector2.down, 1.4f);                            {
                                foreach (RaycastHit2D hit in hitsBelowDoor)
                                {
                                    if (hit.transform.gameObject.GetComponent<RoomScript>() != null)
                                    {
                                        newDoor.transform.parent = hit.transform;
                                        hit.transform.gameObject.GetComponent<RoomScript>().doorsFromRoom.Add(newDoor);
                                        newDoorScript.room = hit.transform.gameObject;
                                        newDoorScript.DoorSide = -1;
                                    }
                                }
                            }
                        }
                        else
                        {
                            RaycastHit2D[] hitsAboveDoor = Physics2D.RaycastAll(newDoor.transform.position, Vector2.up, 1.4f);
                            {
                                foreach (RaycastHit2D hit in hitsAboveDoor)
                                {
                                    if (hit.transform.gameObject.GetComponent<RoomScript>() != null)
                                    {
                                        newDoor.transform.parent = hit.transform;
                                        hit.transform.gameObject.GetComponent<RoomScript>().doorsFromRoom.Add(newDoor);
                                        newDoorScript.room = hit.transform.gameObject;

                                        newDoorScript.DoorSide = 1;
                                    }
                                }
                            }

                        }
                    }
                    else if ((tileLeft == roomFloorTile || tileRight == roomFloorTile) && tileBelow == hallwayFloorTile && isWallTile(tileAbove) && !isWallTile(tileLeft) && !(isWallTile(tileRight)))
                    {
                     //   tileMap.SetTile(pos, horizontalDoorTile);
                        GameObject newDoor = GameObject.Instantiate(HorizontalDoorPrefab);
                        allDoors.Add(newDoor);
                        DoorScript newDoorScript = newDoor.GetComponent<DoorScript>();
                        newDoorScript.DoorType = newDoorScript.HORIZONTAL;
                        newDoor.transform.position = tileMap.CellToWorld(posAbove);
                        if (tileMap.GetTile(posLeft) == roomFloorTile)
                        {
                            RaycastHit2D[] hitsLeft = Physics2D.RaycastAll(newDoor.transform.position, Vector2.left, 1.4f);
                            {
                                foreach (RaycastHit2D hit in hitsLeft)
                                {
                                    if (hit.transform.gameObject.GetComponent<RoomScript>() != null)
                                    {
                                        newDoor.transform.parent = hit.transform;
                                        hit.transform.gameObject.GetComponent<RoomScript>().doorsFromRoom.Add(newDoor);
                                        newDoorScript.room = hit.transform.gameObject;

                                        newDoorScript.DoorSide = -1;

                                    }
                                }
                            }
                        }
                        else
                        {
                            RaycastHit2D[] hitsRight = Physics2D.RaycastAll(newDoor.transform.position, Vector2.right, 1.4f);
                            {
                                foreach (RaycastHit2D hit in hitsRight)
                                {
                                    if (hit.transform.gameObject.GetComponent<RoomScript>() != null)
                                    {
                                        newDoor.transform.parent = hit.transform;
                                        hit.transform.gameObject.GetComponent<RoomScript>().doorsFromRoom.Add(newDoor);
                                        newDoorScript.room = hit.transform.gameObject;

                                        newDoorScript.DoorSide = 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    void GenerateShip()
    {
        Debug.Log("Starting new ship generation");

        shipIsGood = true;
        RoomGameObjects = new List<GameObject>();
        HallwayGameObjects = new List<GameObject>();
        allDoors = new List<GameObject>();
        tileMap.ClearAllTiles();
        Decks = new List<Deck>();
        deckNumber = 0;
        GameObject.Destroy(ship);
        ship = GameObject.Instantiate(ShipPrefab);

        for (int i = 0; i < numberOfDecks; i++)
        {
            GenerateDeck(numberOfRooms, deckNumber);
            deckNumber++;
        }
        FillWalls();
        CombineRoomGameObjects();
        GenerateDoors();
        GenerateHallwayGameObjects();
        GenerateTeleporters();
        GenerateDebris();
        SpawnSpecialDebris();
        SpawnFriend();
        SpawnCouch();
        if (!shipIsGood)
        {
            Debug.Log("Ship is not good.  Regenerating everything.");
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            return;
        }

    }

    public void SpawnSpecialDebris()
    {
        List<GameObject> SpecialDebrisPrefabs = new List<GameObject>();
        SpecialDebrisPrefabs.Add(LavalampPrefab);
        SpecialDebrisPrefabs.Add(FridgePrefab);
        SpecialDebrisPrefabs.Add(CoatPrefab);

        CarryableScript[] carryScripts = Object.FindObjectsOfType<CarryableScript>();
        List<GameObject> allDebris = new List<GameObject>();
        foreach (CarryableScript script in carryScripts)
        {
            allDebris.Add(script.gameObject);
        }

        foreach (GameObject prefabToSpawn in SpecialDebrisPrefabs)
        {
            GameObject objectToReplace = allDebris[Random.Range(0, allDebris.Count)];
            while(objectToReplace.GetComponent<CarryableScript>().isGeneric == false)
            {
                objectToReplace = allDebris[Random.Range(0, allDebris.Count)];
            }

            GameObject newObject = GameObject.Instantiate(prefabToSpawn);
            newObject.transform.parent = objectToReplace.transform.parent;
            newObject.transform.position = objectToReplace.transform.position;
            GameObject.Destroy(objectToReplace);
        }
    }

    public void SpawnFriend()
    {
        GameObject deckToSpawnIn = Decks[2].DeckGameObject;
        bool spawnedYet = false;
        while (spawnedYet == false) {
            Room roomToSpawnIn = Decks[2].rooms[Random.Range(0, Decks[2].rooms.Count)];
            Vector3Int tilePos = new Vector3Int((roomToSpawnIn.GetLeftX() + roomToSpawnIn.GetRightX()) / 2, roomToSpawnIn.GetTopY(), 0);
            GameObject friend = GameObject.Instantiate(FriendPrefab);
            friend.transform.position = tileMap.CellToWorld(tilePos);
            friend.transform.Translate(new Vector3(0, -.5f, 0));
            spawnedYet = true;
         }
    }


    public void SpawnCouch()
    {
        GameObject deckToSpawnIn = Decks[1].DeckGameObject;
        bool spawnedYet = false;
        while (spawnedYet == false)
        {
            Room roomToSpawnIn = Decks[1].rooms[Random.Range(0, Decks[1].rooms.Count)];
            Vector3Int tilePos = new Vector3Int((roomToSpawnIn.GetLeftX() + roomToSpawnIn.GetRightX()) / 2, roomToSpawnIn.GetTopY(), 0);
            GameObject friend = GameObject.Instantiate(CouchPrefab);
            friend.transform.position = tileMap.CellToWorld(tilePos);
            friend.transform.Translate(new Vector3(0, -.5f, 0));
            spawnedYet = true;
        }
    }


    public float chanceOfDebrisSpawning = .01f;

    public void GenerateDebris()
    {
        Vector3 spawnOffset = new Vector3(.5f, .5f, 0);
        BoundsInt bounds = tileMap.cellBounds;
        for (int xMap = bounds.xMin - 10; xMap <= bounds.xMax + 10; xMap++)
        {
            for (int yMap = bounds.yMin - 1; yMap <= bounds.yMax + 1; yMap++)
            {
                Vector3Int pos = new Vector3Int(xMap, yMap, 0);
                Vector3Int posBelow = new Vector3Int(xMap, yMap - 1, 0);
                Vector3Int posAbove = new Vector3Int(xMap, yMap + 1, 0);
                Vector3Int posLeft = new Vector3Int(xMap - 1, yMap, 0);
                Vector3Int posRight = new Vector3Int(xMap + 1, yMap, 0);
                TileBase tile = tileMap.GetTile(pos);
                TileBase tileBelow = tileMap.GetTile(posBelow);
                TileBase tileAbove = tileMap.GetTile(posAbove);
                TileBase tileLeft = tileMap.GetTile(posLeft);
                TileBase tileRight = tileMap.GetTile(posRight);

                if (tile == roomFloorTile)
                {
                    if (Random.value < chanceOfDebrisSpawning*.5f)
                    {
                        GameObject newCrate = GameObject.Instantiate(CratePrefab);
                        newCrate.transform.position = tileMap.CellToWorld(pos)+spawnOffset;
                        

                    }
                    else if (Random.value < chanceOfDebrisSpawning * .5f)
                    {
                        GameObject newBarrel = GameObject.Instantiate(BarrelPrefab);
                        newBarrel.transform.position = tileMap.CellToWorld(pos)+spawnOffset;

                    }
                }
            }
        }
    }


    private void GenerateDeck(int roomCount, int deckNum)
    {
        Deck newDeck = new Deck(deckNum);
        Decks.Add(newDeck);
        newDeck.DeckGameObject = GameObject.Instantiate(DeckPrefab);
        newDeck.DeckGameObject.transform.parent = ship.transform;
        newDeck.DeckGameObject.GetComponent<DeckScript>().DeckNumber = deckNum;
        int iterations = 0;
        //Generate a starting room
        Vector2Int origin = new Vector2Int(0, deckHeight * 2 * deckNum);
        Room currentRoom = GenerateRoom(newDeck);
        if (deckNum == 0) { 
          GameObject Player = GameObject.Instantiate(PlayerPrefab);
            Player.transform.parent = ship.transform;
          Player.name = "Player";
          Vector3Int playerPosition = new Vector3Int(currentRoom.position.x + currentRoom.dimensions.x / 2, currentRoom.position.y + currentRoom.dimensions.y / 2, 0);
          Player.transform.position = tileMap.CellToWorld(playerPosition);
        }
        while (newDeck.rooms.Count < roomCount && iterations < 100)
        {
            iterations++;
            currentRoom = newDeck.rooms[Random.Range(0, newDeck.rooms.Count)];

            //Pick between 1 and 4 directions to make connections
            int numberOfHallways = Random.Range(1, 5);
            int hallwayDirection = Random.Range(0, 4);

            //For each connection, create a hallway going in that direction
            for (int i = 0; i < numberOfHallways; i++)
            {
                if (currentRoom.hasHallway[hallwayDirection] == false)
                {
                    Hallway newHallway = GenerateHallway(currentRoom, hallwayDirection);
                    if (newHallway != null)
                    {
                        /* maybe generate perpendicular rooms here later */
                        currentRoom.hasHallway[hallwayDirection] = true;
                        int roomX = newHallway.getEndX();
                        int roomY = newHallway.getEndY();

                        int roomLength = Random.Range(minRoomLength, maxRoomLength);
                        int roomHeight = Random.Range(minRoomHeight, maxRoomHeight);

                        if (newHallway.direction == WEST)
                        {
                            roomLength *= -1;
                            roomLength++;
                        }
                        if (newHallway.direction == SOUTH)
                        {
                            roomHeight *= -1;
                            roomHeight++;
                        }

                        if (newHallway.direction == WEST || newHallway.direction == EAST)
                        {
                            roomY -= roomHeight / 2;
                        }
                        if (newHallway.direction == NORTH || newHallway.direction == SOUTH)
                        {
                            roomX -= roomLength / 2;
                        }
                        Room connectedRoom = GenerateRoom(newDeck, roomX, roomY, roomLength, roomHeight);
                        if (hallwayDirection == SOUTH)
                        {
                            connectedRoom.hasHallway[NORTH] = true;
                        }
                        if (hallwayDirection == NORTH)
                        {
                            connectedRoom.hasHallway[SOUTH] = true;
                        }
                        if (hallwayDirection == WEST)
                        {
                            connectedRoom.hasHallway[EAST] = true;
                        }
                        if (hallwayDirection == EAST)
                        {
                            connectedRoom.hasHallway[WEST] = true;
                        }
                        newHallway.connectedRooms.Add(connectedRoom);
                    }
                }

                hallwayDirection++;
                if (hallwayDirection > 3)
                {
                    hallwayDirection = 0;
                }
            }
            
        }

        

 
        //For each hallway, pick a length
        //Pick a number of perpendicular rooms
        //Make a room on the other side
        //Generate perpendicular rooms
        //Pick a random room and do it again until we have reached roomCount;
    }

    public bool DoesRoomIntersectWithOtherRooms(Room room)
    {
        foreach (Room roomToCheckAgainst in room.deck.rooms)
        {
            if (roomToCheckAgainst != room)
            {
                if (doRoomsIntersect(room, roomToCheckAgainst))
                {
                    return true;
                }
            }
        }
        return false;
    }

    Hallway GenerateHallway(Room startingRoom, int ConnectionSide)
    {
        int startingX = -1000;
        int startingY = -1000;
        int hallwayLengthX = -1000;
        int hallwayHeightY = -1000;
        int lengthOfHallway = Random.Range(hallwayLengthMin, hallwayLengthMax);
        int widthOfHallway = Random.Range(hallwayWidthMin, hallwayWidthMax);

        if (ConnectionSide == NORTH)
        {
            startingY = startingRoom.GetTopY();
            startingX = startingRoom.GetLeftX() + startingRoom.dimensions.x / 2;
            hallwayLengthX = widthOfHallway;
            hallwayHeightY = lengthOfHallway;
        }
        else if (ConnectionSide == SOUTH)
        {
            startingY = startingRoom.GetBottomY();
            startingX = startingRoom.GetLeftX() + startingRoom.dimensions.x / 2;
            hallwayLengthX = widthOfHallway;
            hallwayHeightY = -lengthOfHallway;
        }
        else if (ConnectionSide == EAST)
        {
            startingX = startingRoom.GetRightX();
            startingY = startingRoom.GetBottomY() + startingRoom.dimensions.y / 2;
            hallwayHeightY = widthOfHallway;
            hallwayLengthX = lengthOfHallway;
        }
        else if (ConnectionSide == WEST)
        {
            startingX = startingRoom.GetLeftX();
            startingY = startingRoom.GetBottomY() + startingRoom.dimensions.y / 2;
            hallwayHeightY = widthOfHallway;
            hallwayLengthX = -lengthOfHallway;
        }

        if (ConnectionSide == NORTH || ConnectionSide == SOUTH)
        {
            if (tileMap.GetTile(new Vector3Int(startingX-1, startingY, 0)) == hallwayFloorTile || 
                tileMap.GetTile(new Vector3Int(startingX+widthOfHallway, startingY, 0)) != null)
            {
                return null;
            }
        }else if (ConnectionSide == EAST || ConnectionSide == WEST)
        {
            if (tileMap.GetTile(new Vector3Int(startingX, startingY-1, 0)) == hallwayFloorTile ||
               tileMap.GetTile(new Vector3Int(startingX, startingY+widthOfHallway, 0)) != null)
            {
                return null;
            }
        }

        Hallway newHallway = new Hallway(startingRoom, ConnectionSide, lengthOfHallway, new Vector2Int(startingX, startingY));
        GenerateFloorRectangle(startingX, startingY, hallwayLengthX, hallwayHeightY, true);
        return newHallway;
    }

    private Room GenerateRoom(Deck deck)
    {
        Vector2Int origin = new Vector2Int(0, deck.deckNumber * deckHeight * 3);
        return GenerateRoom(deck, origin.x, origin.y,
                Mathf.RoundToInt(Random.Range(minRoomLength, maxRoomLength)), Mathf.RoundToInt(Random.Range(minRoomHeight, maxRoomHeight)));
    }


    private Room GenerateRoom(Deck deck, int x, int y, int length, int height)
    {
        Room newRoom = new Room(new Vector2Int(x, y), new Vector2Int(length, height), deck);
        GenerateFloorRectangle(x, y, length, height, false);
        deck.rooms.Add(newRoom);


        if (DoesRoomIntersectWithOtherRooms(newRoom))
        {
            for (int i = 0; i < 4; i++) {
                newRoom.hasHallway[i] = true;
            }
        }
        GameObject newRoomGameObject = GameObject.Instantiate(RoomPrefab);
        newRoomGameObject.transform.position = tileMap.CellToWorld(new Vector3Int(x, y, 0));
        newRoomGameObject.transform.Translate(new Vector2(length/2.0f, height/2.0f));
        BoxCollider2D roomCollider = newRoomGameObject.GetComponent<BoxCollider2D>();
        RoomScript roomScript = newRoomGameObject.GetComponent<RoomScript>();

        RoomGameObjects.Add(newRoomGameObject);
        newRoomGameObject.transform.parent = newRoom.deck.DeckGameObject.transform;
        roomScript.RoomID = nextRoomID;
        nextRoomID++;
        roomCollider.size = new Vector2(length, height);

        return newRoom;
    }

    private void GenerateFloorRectangle(int x, int y, int length, int height, bool isHallway)
    {

        int xDirection = length / Mathf.Abs(length);
        int yDirection = height / Mathf.Abs(height);
        int endingY = y + height;
        int endingX = x + length;
        for (int tileX = x; tileX != endingX; tileX += xDirection)
        {
            for (int tileY = y; tileY != endingY; tileY += yDirection)
            {
                Vector3Int tilePos = new Vector3Int(tileX, tileY, 0);
                if (!isHallway)
                {
                    tileMap.SetTile(tilePos, roomFloorTile);
                }
                else
                {
                    if (tileMap.GetTile(tilePos) != roomFloorTile)
                    {
                        tileMap.SetTile(tilePos, hallwayFloorTile);
                    }
                }
            }
        }        
    }


}