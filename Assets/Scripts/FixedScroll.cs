﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedScroll : MonoBehaviour
{
    public float theScrollSpeed = 0.025f;

    Transform transformToScroll;

    void Start()
    {
        transformToScroll = transform;
    }

    void Update()
    {
        transformToScroll.position = new Vector3(transformToScroll.position.x, transformToScroll.position.y + theScrollSpeed, transformToScroll.position.z);
    }
}
