﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    public int VERTICAL = 0;
    public int HORIZONTAL = 1;
    public int DoorSide;
    public int DoorType;
    public GameObject matchingDoor = null;
    public GameObject attachedHallway = null;
    public bool isOpen = false;
    public GameObject room;
    public Sprite openSprite;
    public Sprite closedSprite;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenDoor()
    {
        isOpen = true;
        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
        attachedHallway.GetComponent<HallwayScript>().TurnOnSpriteMasks();
        GetComponent<SpriteRenderer>().sprite = openSprite;
        room.GetComponent<RoomScript>().TurnOnSpriteMasks();
    }

    public void CloseDoor()
    {
        isOpen = false;
        GetComponent<BoxCollider2D>().enabled = true;
        GetComponent<SpriteRenderer>().enabled = true;
        if (matchingDoor.GetComponent<DoorScript>().isOpen == false)
        {
            if (attachedHallway.GetComponent<HallwayScript>().containsPlayer == false)
            {
                attachedHallway.GetComponent<HallwayScript>().TurnOffSpriteMasks();
            }
        }
        GetComponent<SpriteRenderer>().sprite = closedSprite;

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            OpenDoor();
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            CloseDoor();
        }
    }


}

