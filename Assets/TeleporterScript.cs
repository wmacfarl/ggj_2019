﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleporterScript : MonoBehaviour
{
    public GameObject otherSide;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            PlayerScript playerScript = collision.GetComponent<PlayerScript>();
            if (playerScript.CanTeleport)
            {
                collision.transform.position = otherSide.transform.position + Vector3.down * 2;
                StartCoroutine(playerScript.ActivateTeleport());
            }
        }
    }
}
