﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RoomScript : MonoBehaviour
{
    public int RoomID;
    public List<GameObject> doorsFromRoom;
    public bool containsPlayer = false;


    public List<GameObject> objectsInRoom;

    // Start is called before the first frame update
    void Start()
    {
       // objectsInRoom = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void TurnOnSpriteMasks()
    {
        GameObject parentRoom = gameObject;
        if (transform.parent.GetComponent<RoomScript>() != null)
        {
            parentRoom = transform.parent.gameObject;
        }

        SpriteMask[] spriteMasks = parentRoom.GetComponentsInChildren<SpriteMask>();
        foreach(SpriteMask mask in spriteMasks)
        {
            Vector2 maskScale = mask.gameObject.transform.parent.GetComponent<BoxCollider2D>().bounds.extents*2;
            maskScale += new Vector2(4, 4);
            mask.gameObject.transform.localScale = maskScale;
            mask.enabled = true;
        }


        foreach (GameObject door in doorsFromRoom)
        {
            door.GetComponent<SpriteRenderer>().enabled = true;
        }

        foreach (GameObject thing in objectsInRoom)
        {
            thing.GetComponent<SpriteRenderer>().enabled = true;
        }

    }


    public void TurnOffSpriteMasks()
    {
        GameObject parentRoom = gameObject;
        if (transform.parent.GetComponent<RoomScript>() != null)
        {
            parentRoom = transform.parent.gameObject;
        }

            SpriteMask[] spriteMasks = parentRoom.GetComponentsInChildren<SpriteMask>();
            foreach (SpriteMask mask in spriteMasks)
            {
                mask.enabled = false;
            }

            foreach (GameObject door in doorsFromRoom)
        {
            door.GetComponent<SpriteRenderer>().enabled = false;
        }
        foreach (GameObject thing in objectsInRoom)
        {
            thing.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.GetComponent<PlayerScript>().SwitchRooms(RoomID);
            TurnOnSpriteMasks();
        }
        containsPlayer = true;

    }
    public void OnTriggerExit2D(Collider2D collision)
    {
        containsPlayer = false;

    }


}
