﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarryableScript : MonoBehaviour
{
    public bool isGeneric;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.transform.parent != null)
        {
            if (collision.gameObject.transform.parent.GetComponent<RoomScript>())
            {
                collision.gameObject.transform.parent.GetComponent<RoomScript>().objectsInRoom.Add(gameObject);
            }
        }
        Debug.Log("enteringRoom");
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.transform.parent != null)
        {
            if (collision.gameObject.transform.parent.GetComponent<RoomScript>())
            {
                collision.gameObject.transform.parent.GetComponent<RoomScript>().objectsInRoom.Remove(gameObject);
            }

        }
    }

}
