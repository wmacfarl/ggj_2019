﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HallwayScript : MonoBehaviour
{
    public int HallwayID;
    public GameObject door1;
    public GameObject door2;
    public bool containsPlayer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TurnOnSpriteMasks()
    {

        SpriteMask mask = GetComponentInChildren<SpriteMask>();
            Vector2 maskScale = mask.gameObject.transform.parent.GetComponent<BoxCollider2D>().bounds.extents * 2;
        maskScale += new Vector2(4, 4);
            mask.gameObject.transform.localScale = maskScale;
            mask.enabled = true;
        door1.GetComponent<SpriteRenderer>().enabled = true;
        door2.GetComponent<SpriteRenderer>().enabled = true;
    }


    public void TurnOffSpriteMasks()
    {
        SpriteMask mask = GetComponentInChildren<SpriteMask>();
        mask.enabled = false;
        if (door1.GetComponent<DoorScript>().room.GetComponent<RoomScript>().containsPlayer == false)
        {
            door1.GetComponent<SpriteRenderer>().enabled = false;
        }
        if (door2.GetComponent<DoorScript>().room.GetComponent<RoomScript>().containsPlayer == false) { 
            door2.GetComponent<SpriteRenderer>().enabled = false;
    }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            containsPlayer = true;
        }
    }
    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            containsPlayer = false;
        }
    }
}
